<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use AppBundle\Entity\User;
use AppBundle\Entity\GameQueue;
use AppBundle\Game\GameSecurity;
use AppBundle\Game\GamePlaySecurity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GameController extends Controller
{
    public function gameAction(Request $request)
    {
        $id = $this->getUser()->getId();

        $repositoryGame = $this->getDoctrine()->getRepository(Game::class);

        if($repositoryGame->findByIdEnemy($id)) {
            return $this->json(array('result' => true));
        }

        $repository = $this->getDoctrine()->getRepository(GameQueue::class);

        $query = $repository->createQueryBuilder('p')
            ->where('p.userId != :id')
            ->setParameter('id',$id)
            ->getQuery();

        $gameUserArray = $query->getResult();

        if ($gameUserArray) {

            $enemy = array_shift ($gameUserArray);
            $enemyId = $enemy->getUserId();

            for($i = 0; $i < 10; $i++){
                for($j = 0; $j < 10; $j++){
                    $gameProcess[$i][$j] = 0;
                }
            }

            $gameShip = [
                'Разместите трехпалубник (2)' => 3,
                'Разместите трехпалубник (1)' => 3,
                'Разместите четырехпалубник (1)' => 4,
                'Разместите двухпалубник (3)' => 2,
                'Разместите двухпалубник (2)' => 2,
                'Разместите двухпалубник (1)' => 2,
                'Разместите однопалубник (4)' => 1,
                'Разместите однопалубник (3)' => 1,
                'Разместите однопалубник (2)' => 1,
                'Разместите однопалубник (1)' => 1,
            ];

            $gameSetting ['process'] = $gameProcess;
            $gameSetting ['ship'] = $gameShip;

            $gameSetting = serialize($gameSetting);

            $game1 = new Game();
            $game1->setIdEnemy($enemyId);
            $game1->setIdUser($id);
            $game1->setGame($gameSetting);

            $game2 = new Game();
            $game2->setIdEnemy($id);
            $game2->setIdUser($enemyId);
            $game2->setGame($gameSetting);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($game1);
            $entityManager->persist($game2);
            $entityManager->remove($enemy);
            $entityManager->flush();

            $result = true;
        } else {
            $query = $repository->createQueryBuilder('p')
                ->where('p.userId = :id')
                ->setParameter('id',$id)
                ->getQuery();

            if(!$query->getResult()){
                $gameQueue = new GameQueue();
                $gameQueue->setUserId($id);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($gameQueue);
                $entityManager->flush();
            }

            $result = false;
        }

       return $this->json(array('result' => $result));
    }

    public function gameProcessSecurityAction(Request $request)
    {
        $id = $this->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $gameArray = $entityManager->getRepository(Game::class)->findByIdUser($id);

        $game = unserialize($gameArray[0]->getGame());

        $satrt = ($request->get('start'));

        if($satrt){
            foreach ($game['ship'] as $key => $value) {
                return $this->json(array('result' => $key));
            }
        }

        $idArray = json_decode($request->get('id'));

        $gameSecurity = new GameSecurity($idArray, $game);
        $result = $gameSecurity->startSecurity();

        if(!$result)return $this->json(array('result' => false));

        if(count($result['ship']) == 0){
            $result['ship'] = [
                1 => 4,
                2 => 3,
                3 => 2,
                4 => 1,
            ];

            $game = serialize($result);
            $gameArray[0]->setGame($game);
            $entityManager->flush();
            return $this->json(array('result' => "play"));
        }

        $game = serialize($result);
        $gameArray[0]->setGame($game);
        $entityManager->flush();

        foreach ($result['ship'] as $key => $value) {
            return $this->json(array('result' => $key));
        }
    }

    public function gamePlayQueryAction()
    {
        $id = $this->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $userPlay = $entityManager->getRepository(Game::class)->findByIdUser($id);
        $userPlay[0]->setGameReady(true);
        $entityManager->flush();

        $enemyPlay = $entityManager->getRepository(Game::class)->findByIdEnemy($id);
        $enemyPlay = $enemyPlay[0];

        if($enemyPlay->getGameReady() === true){

            if($enemyPlay->getStroke() != true) {
                $userPlay[0]->setStroke(true);
                $entityManager->flush();
            }

            return $this->json(array('result' => true));
        }

        return $this->json(array('result' => false));
    }

    public function gamePlaySecurityAction(Request $request)
    {
        $id = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();

        $userGame = $entityManager->getRepository(Game::class)->findByIdUser($id);

        $gameArray = $entityManager->getRepository(Game::class)->findByIdEnemy($id);
        $game = unserialize($gameArray[0]->getGame());

        $idField = ($request->get('id'));

        $h = (int)substr("$idField ", 0, 1);
        $v = (int)substr("$idField ", 1, 2);

        if($game['process'][$h][$v] === 0){
            $game['process'][$h][$v] = "slip";
            $game = serialize($game);
            $gameArray[0]->setGame($game);

            $userGame[0]->setStroke(false);
            $gameArray[0]->setStroke(true);
            $entityManager->flush();

            return $this->json(array('result' => true));
        }

        $gamePlaySecurity = new GamePlaySecurity($idField, $game);
        $result =  $gamePlaySecurity->startSecurity();

        $countShip = 0;

        foreach ($result['ship'] as $value){
            if($value > 0)$countShip++;
        }

        if($countShip == 0){
            $userGame[0]->setVictory(true);
            $entityManager->flush();

            $repository = $entityManager->getRepository(User::class);
            $user = $repository->findOneById($id);

            $user->getUserStatistics()->setGamesNumber(
                $user->getUserStatistics()->getGamesNumber()+1);
            $user->getUserStatistics()->setVictory(
                $user->getUserStatistics()->getVictory()+1);

            $entityManager->flush();

            return $this->json(array('result' => 'victory'));
        }

        $game = serialize($result);
        $gameArray[0]->setGame($game);

        $entityManager->flush();

        return $this->json(array('result' => true));
    }

    public function gamePlayStrokeAction(Request $request)
    {
        $id = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();

        $enemyGame = $entityManager->getRepository(Game::class)->findByIdEnemy($id);
        if($enemyGame[0]->getVictory() == true){
            $repository = $entityManager->getRepository(User::class);
            $user = $repository->findOneById($id);

            $user->getUserStatistics()->setGamesNumber(
                $user->getUserStatistics()->getGamesNumber()+1);

            $userGame = $entityManager->getRepository(Game::class)->findByIdUser($id);

            $entityManager->remove($enemyGame[0]);
            $entityManager->remove($userGame[0]);

            $entityManager->flush();

            return $this->json(array('result' => "gameOver"));
        }

        $userGame = $entityManager->getRepository(Game::class)->findByIdUser($id);
        if($userGame[0]->getStroke() != true){
            return $this->json(array('result' => false));
        }

        return $this->json(array('result' => true));
    }

    public function gameUserFieldAction(Request $request)
    {
        $id = $this->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $gameArray = $entityManager->getRepository(Game::class)->findByIdUser($id);

        $game = unserialize($gameArray[0]->getGame());

        return $this->render('game/my_table.html.twig', array(
            'gameArray' => $game['process'],
        ));
    }

    public function gameEnemyFieldAction(Request $request)
    {
        $id = $this->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $gameArray = $entityManager->getRepository(Game::class)->findByIdEnemy($id);

        $game = unserialize($gameArray[0]->getGame());

        return $this->render('game/enemy_table.html.twig', array(
            'gameArray' => $game['process'],
        ));
    }

    public function gamePlayAction(Request $request)
    {
        $id = $this->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $gameArray = $entityManager->getRepository(Game::class)->findByIdUser($id);

        $game = unserialize($gameArray[0]->getGame());

        $enemy = $entityManager->getRepository(User::class);
        $enemy = $enemy->findOneById($gameArray[0]->getIdEnemy());

        $img = $enemy->getUserInfo()->getImg();
        $username = $enemy->getUsername();
        $games = $enemy->getUserStatistics()->getGamesNumber();
        $victory = $enemy->getUserStatistics()->getVictory();

        return $this->render('game/game_play.html.twig', array(
            'gameArray' => $game['process'],
            'img' => $img,
            'username' => $username,
            'games' => $games,
            'victory' => $victory,
            'id' => $gameArray[0]->getIdEnemy(),
        ));
    }

    public function gameProcessAction(Request $request)
    {
        $id = $this->getUser()->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $gameArray = $entityManager->getRepository(Game::class)->findByIdUser($id);

        $game = unserialize($gameArray[0]->getGame());

        $counter =0;
        foreach ($game['ship'] as $key=>$value){
            $counter += $key;
        };

        return $this->render('game/game.html.twig', array(
            'gameArray' => $game['process'],
            'counterNotNull' => 0,
            'counter' => $counter,
        ));
    }

    public function gameStartAction(Request $request)
    {
        return $this->render('game/game_start.html.twig');
    }

    public function gameOverAction(Request $request)
    {
        return $this->render('game/game_over.html.twig');
    }

    public function gameVictoryAction(Request $request)
    {
        return $this->render('game/game_victory.html.twig');
    }

}
