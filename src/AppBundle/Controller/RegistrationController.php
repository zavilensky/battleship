<?php

namespace AppBundle\Controller ;

use AppBundle\Entity\UserInfo;
use AppBundle\Entity\UserStatistics ;
use AppBundle\Entity\User ;
use AppBundle\Form\UserType ;
use Symfony\Bundle\FrameworkBundle\Controller\Controller ;
use Symfony\Component\HttpFoundation\Request ;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface ;

class RegistrationController extends Controller
{
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userInfo = new UserInfo();
            $userInfo->setImg('avatar.png');

            $userStatistics = new  UserStatistics();
            $userStatistics->setVictory(0);
            $userStatistics->setGamesNumber(0);

            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setUserInfo($userInfo);
            $user->setUserStatistics($userStatistics);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userInfo);
            $entityManager->persist($user);
            $entityManager->persist($userStatistics);
            $entityManager->flush();

            return $this->redirectToRoute('battleship_login');
        }

        return $this->render(
            'authentication/register.html.twig',
            array('form' => $form->createView())
        );
    }
}