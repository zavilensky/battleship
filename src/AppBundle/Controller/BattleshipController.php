<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserInfoType ;
use AppBundle\Entity\UserInfo ;
use AppBundle\Entity\UserStatistics ;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BattleshipController extends Controller
{
    public function userInfoAction($id, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(UserInfo::class);
        $userInfo = $repository->findOneById($id);

        $repository = $this->getDoctrine()->getRepository(UserStatistics::class);
        $userStatistics = $repository->findOneById($id);

        if($this->getUser()->getId() == $id)$button = true;
        else $button = false;

        return $this->render('battleship/userinfo.html.twig', [
            'userInfo' => $userInfo,
            'userStatistics' =>  $userStatistics,
            'button' => $button,
        ]);
    }

    public function regulationsAction(Request $request)
    {
        return $this->render('battleship/regulations.html.twig');
    }

    public function homePageAction(Request $request)
    {
        $id = $this->getUser()->getId();

        return $this->redirectToRoute('user_info', array('id' => $id));
    }

    public function userInfoSettingAction(Request $request)
    {
        $id = $this->getUser()->getId();

        $repository = $this->getDoctrine()->getRepository(UserInfo::class);
        $user = $repository->findOneById($id);
        $img = $user->getImg();

        $form = $this->createForm(UserInfoType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if (!$user->getImg()){
                $user->setImg($img);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('home_page');
        }

        $img =$this->getUser()->getUserInfo()->getImg();


        return $this->render(
            'battleship/userinfo_setting.html.twig', array(
                'form' => $form->createView(),
                'img' => $img,
            )
        );
    }
}
