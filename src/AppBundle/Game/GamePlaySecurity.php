<?php

namespace AppBundle\Game;

class GamePlaySecurity
{
    private $game;
    private $id;

    public function __construct($id, $game)
    {
        $this->game = $game;
        $this->id = $id;
    }

    public function startSecurity()
    {
        $process = $this->game['process'];
        $ship = $this->game['ship'];

        $h = substr("$this->id", 0, 1);
        $v = substr("$this->id", 1, 2);

        if( $process[$h][$v] === 1){
            $process[$h][$v] = "killed";
            $ship[1]--;
            return [
                'process' => $process,
                'ship' => $ship,
            ];
        }

        $Counter = $this->neighborsFilter($h, $v, $process);

        if($Counter < $process[$h][$v]-1){
            $process[$h][$v] = "injured";

            return [
                'process' => $process,
                'ship' => $ship,
            ];
        }

        $ship[$process[$h][$v]]--;
        $process[$h][$v] = "killed";
        $process = $this->killed($h, $v, $process);

        return [
            'process' => $process,
            'ship' => $ship,
        ];
    }

    private function neighborsFilter($horizontal, $vertical, $process)
    {
        $Counter = 0;

        for ($i = $horizontal+1; $i<10; $i++){
                if($process[$i][$vertical] === 0 || $process[$i][$vertical] == "slip")break;

                if($process[$i][$vertical] == "injured"){
                    $Counter++;
                }
        }

        for ($i = $horizontal-1; $i >= 0; $i--){
            if($process[$i][$vertical] === 0 || $process[$i][$vertical] == "slip")break;

            if($process[$i][$vertical] == "injured"){
                $Counter++;
            }
        }

        for ($i = $vertical+1; $i < 10; $i++){
            if($process[$horizontal][$i] === 0 || $process[$horizontal][$i] == "slip")break;

            if($process[$horizontal][$i] == "injured"){
                $Counter++;
            }
        }

        for ($i = $vertical-1; $i >= 0; $i--){
            if($process[$horizontal][$i] === 0 || $process[$horizontal][$i] == "slip")break;

            if($process[$horizontal][$i] == "injured"){
                $Counter++;
            }
        }

        return $Counter;
    }

    private function killed($horizontal, $vertical, $process)
    {
        for ($i = $horizontal+1; $i<10; $i++){
            if($process[$i][$vertical] === 0 || $process[$i][$vertical] == "slip" )break;

            $process[$i][$vertical] = "killed";
        }

        for ($i = $horizontal-1; $i >= 0; $i--){
            if($process[$i][$vertical] === 0 || $process[$i][$vertical] == "slip")break;

            $process[$i][$vertical] = "killed";
        }

        for ($i = $vertical+1; $i < 10; $i++ ){
            if($process[$horizontal][$i] === 0 || $process[$horizontal][$i] == "slip")break;

            $process[$horizontal][$i] = "killed";
        }

        for ($i = $vertical-1; $i >= 0; $i--){
            if($process[$horizontal][$i] === 0 || $process[$horizontal][$i] == "slip")break;

            $process[$horizontal][$i] = "killed";
        }

        return $process;
    }


}
