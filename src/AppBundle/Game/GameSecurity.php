<?php

namespace AppBundle\Game;

class GameSecurity
{
    private $game;
    private $idArray;

    public function __construct($idArray, $game)
    {
        $this->game = $game;
        $this->idArray = $idArray;
    }

    public function startSecurity()
    {
        $process = $this->game['process'];
        $ship = $this->game['ship'];

        $value = array_shift($ship);

        if (count($this->idArray) != $value) {
            return false;
        }

        foreach ($this->idArray as $key => $id){
            $horizontal = substr("$id", 0,1);
            $vertical = substr("$id", 1,2);

            $process[$horizontal][$vertical] = $value;

            if($key+1 < count($this->idArray) ){

                if (abs($id - $this->idArray[$key+1]) != 10 and
                    abs($id - $this->idArray[$key+1]) != 1)return false;
            }
        }

        foreach ($this->idArray as $id){
            if(!$this->diagonalFilter($id, $process))return false;
        }

        if(!$this->neighborsFilter($this->idArray[0], $process))return false;
        if(!$this->neighborsFilter($this->idArray[count($this->idArray)-1], $process))return false;

        return [
            'process' => $process,
            'ship' => $ship,
        ];
    }

    private function diagonalFilter($id, $process)
    {

        $horizontal = substr("$id", 0,1);
        $vertical = substr("$id", 1,2);

        if ($horizontal+1 < 10 and $vertical+1 < 10){
            if($process[$horizontal+1][$vertical+1] !== 0){
                return false;
            }
        }

        if ($horizontal-1 > 0 and $vertical-1 >= 0){
            if($process[$horizontal-1][$vertical-1] !== 0){
                return false;
            }
        }

        if ($horizontal+1 < 10 and $vertical-1 >= 0){
            if($process[$horizontal+1][$vertical-1] !== 0){
                return false;
            }
        }

        if ($horizontal-1 > 0 and $vertical+1 < 10){
            if($process[$horizontal-1][$vertical+1] !== 0){
                return false;
            }
        }

        return true;
    }

    private function neighborsFilter($id, $process)
    {

        $horizontal = substr("$id", 0,1);
        $vertical = substr("$id", 1,2);

        $Counter = 0;

        if ($horizontal+1 < 10){
            if($process[$horizontal+1][$vertical] !== 0){
                $Counter++;
            }
        }

        if ($horizontal-1 > 0 ){
            if($process[$horizontal-1][$vertical] !== 0){
                $Counter++;
            }
        }

        if ($vertical-1 >= 0){
            if($process[$horizontal][$vertical-1] !== 0){
                $Counter++;
            }
        }

        if ($vertical+1 < 10){
            if($process[$horizontal][$vertical+1] !== 0){
                $Counter++;
            }
        }

        if($process[$horizontal][$vertical] === 1){
            if($Counter > 0)return false;
        }
        if($Counter > 1)return false;

        return true;
    }

}
