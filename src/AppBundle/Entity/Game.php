<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_enemy", type="integer")
     */
    private $idEnemy;

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer")
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="game", type="text")
     */
    private $game;

    /**
     * @var boolean
     *
     * @ORM\Column(name="game_ready", type="boolean", nullable=true)
     */
    private $gameReady;

    /**
     * @var boolean
     *
     * @ORM\Column(name="stroke", type="boolean", nullable=true)
     */
    private $stroke;

    /**
     * @var boolean
     *
     * @ORM\Column(name="victory", type="boolean", nullable=true)
     */
    private $victory;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEnemy
     *
     * @param integer $idEnemy
     *
     * @return Game
     */
    public function setIdEnemy($idEnemy)
    {
        $this->idEnemy = $idEnemy;

        return $this;
    }

    /**
     * Get idEnemy
     *
     * @return int
     */
    public function getIdEnemy()
    {
        return $this->idEnemy;
    }

    /**
     * Set idUser
     *
     * @param integer $idUser
     *
     * @return Game
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set game
     *
     * @param string $game
     *
     * @return Game
     */
    public function setGame($game)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return string
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set gameReady
     *
     * @param boolean $gameReady
     *
     * @return Game
     */
    public function setGameReady($gameReady)
    {
        $this->gameReady = $gameReady;

        return $this;
    }

    /**
     * Get gameReady
     *
     * @return boolean
     */
    public function getGameReady()
    {
        return $this->gameReady;
    }

    /**
     * Set stroke
     *
     * @param boolean $stroke
     *
     * @return Game
     */
    public function setStroke($stroke)
    {
        $this->stroke = $stroke;

        return $this;
    }

    /**
     * Get stroke
     *
     * @return boolean
     */
    public function getStroke()
    {
        return $this->stroke;
    }

    /**
     * Set victory
     *
     * @param boolean $victory
     *
     * @return Game
     */
    public function setVictory($victory)
    {
        $this->victory = $victory;

        return $this;
    }

    /**
     * Get victory
     *
     * @return boolean
     */
    public function getVictory()
    {
        return $this->victory;
    }
}
