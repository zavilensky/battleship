<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserStatistics
 *
 * @ORM\Table(name="user_statistics")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserStatisticsRepository")
 */
class UserStatistics
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="victory", type="integer", nullable=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $victory;

    /**
     * @var int
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="games_number", type="integer", nullable=true)
     */
    private $gamesNumber;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="UserStatistics")
     */
    private $User ;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set victory
     *
     * @param integer $victory
     *
     * @return UserStatistics
     */
    public function setVictory($victory)
    {
        $this->victory = $victory;

        return $this;
    }

    /**
     * Get victory
     *
     * @return int
     */
    public function getVictory()
    {
        return $this->victory;
    }

    /**
     * Set gamesNumber
     *
     * @param integer $gamesNumber
     *
     * @return UserStatistics
     */
    public function setGamesNumber($gamesNumber)
    {
        $this->gamesNumber = $gamesNumber;

        return $this;
    }

    /**
     * Get gamesNumber
     *
     * @return int
     */
    public function getGamesNumber()
    {
        return $this->gamesNumber;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UserStatistics
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }
}
