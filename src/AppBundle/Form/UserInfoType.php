<?php

namespace AppBundle\Form;

use AppBundle\Entity\UserInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType ;


class UserInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('gender')
            ->add('dateOfBrith',DateType::class, array (
                'widget' => 'single_text',
                'required' => false,
            ))
            ->add('hobbies')
            ->add('status')
            ->add('img',FileType::class, array(
                'required' => false,
                'data' => false ,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver -> setDefaults(array (
            'data_class' => UserInfo::class ,
        ));
    }
}